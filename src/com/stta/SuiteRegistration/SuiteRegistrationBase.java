package com.stta.SuiteRegistration;

import java.io.IOException;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import com.stta.TestSuiteBase.SuiteBase;
import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class SuiteRegistrationBase extends SuiteBase{
	//This function will be executed before SuiteRegistration's test cases to check SuiteToRun flag.
	@BeforeSuite
	public void checkSuiteToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();	
		//To set TestSuiteList.xls file's path In filePath Variable.
		Read_XLS filePath = testSuiteListExcel;
		String sheetName = "SuitesList";
		String suiteName = "SuiteRegistration";
		String toRunColumnName = "SuiteToRun";
		//Called runOrSkipSuite function to run or skip suite
		runOrSkipSuite(filePath, sheetName, suiteName, toRunColumnName);		
	}	
}
