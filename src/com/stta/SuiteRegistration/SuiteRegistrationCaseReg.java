//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package com.stta.SuiteRegistration;

import java.io.IOException;

import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

//SuiteAccountCaseModify Class Inherits From SuiteAccountBase Class.
//So, SuiteAccountCaseModify Class Is Child Class Of SuiteAccountBase Class And SuiteBase Class.
public class SuiteRegistrationCaseReg extends SuiteRegistrationBase{
	Read_XLS filePath = null;	
	String sheetName = null;
	String testCaseName = null;	
	String toRunColumnNameTestCase = null;
	String toRunColumnNameTestData = null;
	String testDataToRun[]=null;
	static boolean TestCasePass=true;
	static int dataSet=-1;	
	static boolean testSkip=false;
	static boolean testFail=false;
	SoftAssert s_assert =null;
	
	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();	
		//To set SuiteRegistration.xls file's path In filePath Variable.
		filePath = testCaseListExcelTwo;		
		testCaseName = this.getClass().getSimpleName();
		//sheetName to check CaseToRun flag against test case.
		sheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		toRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		toRunColumnNameTestData = "DataToRun";
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(testCaseName+" : Execution started.");
		
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
		if(!SuiteUtility.checkToRunUtility(filePath, sheetName,toRunColumnNameTestCase,testCaseName)){			
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(testCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+testCaseName);
		}
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		testDataToRun = SuiteUtility.checkToRunUtilityOfData(filePath, testCaseName, toRunColumnNameTestData);
	}
	
	//Accepts columns String data In every Iteration.
	@Test(dataProvider="SuiteRegistrationCaseRegData")
	public void SuiteRegistrationCaseRegTest(String DataCol1,String DataCol2,String DataCol3,String DataCol4,String DataCol5,String DataCol6,String expectedResult){
		
		dataSet++;
		
		//Created object of testng SoftAssert class.
		s_assert = new SoftAssert();		
				
		if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){	
			Add_Log.info(testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set testSkip=true.
			testSkip=true;
			throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
		
		/*
		//To Convert data from String to Integer
		int ValueOne = Integer.parseInt(DataCol1);
		*/	
		
		//To Initialize browser.
		loadWebBrowser();
		
		//To navigate to URL. It will read site URL from Param.properties file	
		openWebBrowser("ChatGUI/registration/registration.view.html");
		
		testFail = compareTitle(testFail, "Registration", s_assert);
		
		//set values to the fields
		getElementByID("first_name").sendKeys(DataCol1);
		getElementByID("last_name").sendKeys(DataCol2);
		getElementByID("email").sendKeys(DataCol3);
		getElementByID("password").sendKeys(DataCol4);
		getElementByID("password_confirm").sendKeys(DataCol5);
		//click on the register button
		getElementByID("btn_register_user").click();
		
		//click ok on the alert pop up window
		String alertText = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		Add_Log.debug("Alert accepted: " + alertText);
		
		//validate that system is on the correct page
		testFail = compareTitle(testFail, expectedResult, s_assert);
		
	}
	
	//@AfterMethod method will be executed after execution of @Test method every time.
	@AfterMethod
	public void reporterDataResults(){		
		if(testSkip){
			//If found testSkip = true, Result will be reported as SKIP against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "SKIP");
		}	
		else if(testFail){
			//To make object reference null after reporting In report.
			s_assert = null;
			//Set TestCasePass = false to report test case as fail In excel sheet.
			TestCasePass=false;
			//If found testFail = true, Result will be reported as FAIL against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "FAIL");			
		}
		else{
			//If found testSkip = false and testFail = false, Result will be reported as PASS against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "PASS");
		}
		//At last make both flags as false for next data set.
		testSkip=false;
		testFail=false;
	}
	
	//This data provider method will return 3 column's data one by one In every Iteration.
	@DataProvider
	public Object[][] SuiteRegistrationCaseRegData(){
		//To retrieve data from Data 1 Column,Data 2 Column and Expected Result column of SuiteAccountCaseModify data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(filePath, testCaseName);
	}

	//To report result as pass or fail for test cases In TestCasesList sheet.
	@AfterTest
	public void closeBrowser(){
		//To Close the web browser at the end of test.
		closeWebBrowserAddLog(testCaseName, TestCasePass, filePath, sheetName);		
	}
}