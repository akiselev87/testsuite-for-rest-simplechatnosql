//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package com.stta.TestSuiteBase;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.SkipException;
import org.testng.asserts.SoftAssert;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class SuiteBase {	
	public static Read_XLS testSuiteListExcel=null;
	public static Read_XLS testCaseListExcelOne=null;
	public static Read_XLS testCaseListExcelTwo=null;
	public static Read_XLS testCaseListExcelThree=null;
	public static Read_XLS testCaseListExcelFour=null;
	public static Logger Add_Log = null;
	public boolean browserAlreadyLoaded=false;
	public static Properties Param = null;
	public static Properties Object = null;
	public static WebDriver driver=null;
	public static WebDriver existingchromeBrowser;
	public static WebDriver existingmozillaBrowser;
	public static WebDriver existingIEBrowser;
	
	public void init() throws IOException{
		//To Initialize logger service.
		Add_Log = Logger.getLogger("rootLogger");				
				
		//Please change file's path strings bellow If you have stored them at location other than bellow.
		//Initializing Test Suite List(TestSuiteList.xls) File Path Using Constructor Of Read_XLS Utility Class.
		testSuiteListExcel = new Read_XLS(System.getProperty("user.dir")+"\\src\\com\\stta\\ExcelFiles\\TestSuiteList.xls");
		//Initializing Test Suite One(SuiteLogin.xls) File Path Using Constructor Of Read_XLS Utility Class.
		testCaseListExcelOne = new Read_XLS(System.getProperty("user.dir")+"\\src\\com\\stta\\ExcelFiles\\SuiteLogin.xls");
		//Initializing Test Suite Two(SuiteRegistration.xls) File Path Using Constructor Of Read_XLS Utility Class.
		testCaseListExcelTwo = new Read_XLS(System.getProperty("user.dir")+"\\src\\com\\stta\\ExcelFiles\\SuiteRegistration.xls");
		//Initializing Test Suite Three(SuiteAccount.xls) File Path Using Constructor Of Read_XLS Utility Class.
		testCaseListExcelThree = new Read_XLS(System.getProperty("user.dir")+"\\src\\com\\stta\\ExcelFiles\\SuiteAccount.xls");
		//Initializing Test Suite Three(SuiteMessage.xls) File Path Using Constructor Of Read_XLS Utility Class.
		testCaseListExcelFour = new Read_XLS(System.getProperty("user.dir")+"\\src\\com\\stta\\ExcelFiles\\SuiteMessage.xls");
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info("All Excel Files Initialised successfully.");
		
		//Initialize Param.properties file.
		Param = new Properties();
		FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+"//src//com//stta//property//Param.properties");
		Param.load(fip);
		Add_Log.info("Param.properties file loaded successfully.");		
	
		//Initialize Objects.properties file.
		Object = new Properties();
		fip = new FileInputStream(System.getProperty("user.dir")+"//src//com//stta//property//Objects.properties");
		Object.load(fip);
		Add_Log.info("Objects.properties file loaded successfully.");
	}
	
	public String[] runOrSkipCase(Read_XLS filePath, String testCaseName, String sheetName, String toRunColumnNameTestCase, String toRunColumnNameTestData) {
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
		if(!SuiteUtility.checkToRunUtility(filePath, sheetName,toRunColumnNameTestCase,testCaseName)){
			Add_Log.info(testCaseName+" : CaseToRun = N for So Skipping Execution.");
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(testCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+testCaseName);
		}	
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		return SuiteUtility.checkToRunUtilityOfData(filePath, testCaseName, toRunColumnNameTestData);
	}
	
	public void openWebBrowser(String pathURL){
		driver.get(Param.getProperty("siteURL")+ pathURL);	
		driver.manage().window().maximize();
	}
	
	public void loadWebBrowser(){
		//Check If any previous webdriver browser Instance Is exist then run new test In that existing webdriver browser Instance.
			if(Param.getProperty("testBrowser").equalsIgnoreCase("Mozilla") && existingmozillaBrowser!=null){
				driver = existingmozillaBrowser;
				return;
			}else if(Param.getProperty("testBrowser").equalsIgnoreCase("chrome") && existingchromeBrowser!=null){
				driver = existingchromeBrowser;
				return;
			}else if(Param.getProperty("testBrowser").equalsIgnoreCase("IE") && existingIEBrowser!=null){
				driver = existingIEBrowser;
				return;
			}		
		
		
			if(Param.getProperty("testBrowser").equalsIgnoreCase("Mozilla")){
				//To Load Firefox driver Instance. 
				driver = new FirefoxDriver();
				existingmozillaBrowser=driver;
				Add_Log.info("Firefox Driver Instance loaded successfully.");
				
			}else if(Param.getProperty("testBrowser").equalsIgnoreCase("Chrome")){
				//To Load Chrome driver Instance.
				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//BrowserDrivers//chromedriver.exe");
				driver = new ChromeDriver();
				existingchromeBrowser=driver;
				Add_Log.info("Chrome Driver Instance loaded successfully.");
				
			}else if(Param.getProperty("testBrowser").equalsIgnoreCase("IE")){
				//To Load IE driver Instance.
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"//BrowserDrivers//IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				existingIEBrowser=driver;
				Add_Log.info("IE Driver Instance loaded successfully.");
				
			}			
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.manage().window().maximize();			
	}

	//check if we should run suite or skip it
	public void runOrSkipSuite(Read_XLS filePath, String sheetName, String suiteName, String toRunColumnName) {
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info("Execution started for " + suiteName + ".");
		
		//If SuiteToRun !== "y" then SuiteLogin will be skipped from execution.
		if(!SuiteUtility.checkToRunUtility(filePath, sheetName,toRunColumnName,suiteName)){			
			Add_Log.info("SuiteToRun = N for "+suiteName+" So Skipping Execution.");
			//To report SuiteLogin as 'Skipped' In SuitesList sheet of TestSuiteList.xls If SuiteToRun = N.
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Skipped/Executed", suiteName, "Skipped");
			//It will throw SkipException to skip test suite's execution and suite will be marked as skipped In testng report.
			throw new SkipException(suiteName+"'s SuiteToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+suiteName);
		}
		//To report Suite as 'Executed' In SuitesList sheet of TestSuiteList.xls If SuiteToRun = Y.
		SuiteUtility.WriteResultUtility(filePath, sheetName, "Skipped/Executed", suiteName, "Executed");
	}
	
	public void afterMethodCheckStatus(boolean testskip, boolean testfail, Read_XLS filePath, String testCaseName, int dataSet, SoftAssert s_assert, boolean testCasePass) {
		if(testskip){
			Add_Log.info(testCaseName+" : Reporting test data set line "+(dataSet+1)+" as SKIP In excel.");
			//If found testSkip = true, Result will be reported as SKIP against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "SKIP");
		}
		else if(testfail){
			Add_Log.info(testCaseName+" : Reporting test data set line "+(dataSet+1)+" as FAIL In excel.");
			//To make object reference null after reporting In report.
			s_assert = null;
			//Set TestCasePass = false to report test case as fail In excel sheet.
			testCasePass=false;	
			//If found testFail = true, Result will be reported as FAIL against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "FAIL");			
		}else{
			Add_Log.info(testCaseName+" : Reporting test data set line "+(dataSet+1)+" as PASS In excel.");
			//If found testSkip = false and testFail = false, Result will be reported as PASS against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "PASS");
		}
	}
	

	//close browser
	public void closeWebBrowser(){
		driver.close();
		//null browser Instance when close.
		existingchromeBrowser=null;
		existingmozillaBrowser=null;
		existingIEBrowser=null;
	}
	
	//add logs while closing the browser
	public void closeWebBrowserAddLog(String testCaseName, boolean testCasePass, Read_XLS filePath, String sheetName){
		closeWebBrowser();
		if(testCasePass){
			Add_Log.info(testCaseName+" : Reporting test case as PASS In excel.");
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "PASS");
		}
		else{
			Add_Log.info(testCaseName+" : Reporting test case as FAIL In excel.");
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "FAIL");			
		}
	}
	
	//getElementByXPath function for static xpath
	public WebElement getElementByXPathStatic(String Key){
		try{
			//This block will find element using Key value from web page and return It.
			return driver.findElement(By.xpath(Object.getProperty(Key)));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//getElementByXPath function for dynamic xpath
	public WebElement getElementByXPathDynamic(String Key1, int val, String key2){
		try{
			//This block will find element using values of Key1, val and key2 from web page and return It.
			return driver.findElement(By.xpath(Object.getProperty(Key1)+val+Object.getProperty(key2)));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.debug("Object not found for custom xpath");
			return null;
		}
	}
	
	//Call this function to locate element by ID locator.
	public WebElement getElementByID(String Key){
		try{
			return driver.findElement(By.id(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by Name Locator.
	public WebElement getElementByName(String Key){
		try{
			return driver.findElement(By.name(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by cssSelector Locator.
	public WebElement getElementByCSS(String Key){
		try{
			return driver.findElement(By.cssSelector(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by ClassName Locator.
	public WebElement getElementByClass(String Key){
		try{
			return driver.findElement(By.className(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by tagName Locator.
	public WebElement getElementByTagName(String Key){
		try{
			return driver.findElement(By.tagName(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by link text Locator.
	public WebElement getElementByFullLinkText(String Key){
		try{
			return driver.findElement(By.linkText(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by partial link text Locator.
	public WebElement getElementByLinkText(String Key){
		try{
			return driver.findElement(By.partialLinkText(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//check that error message displays on the page
	public boolean validateMessageExists(SoftAssert s_assert, boolean testfail, String DataColX, String emailErrorMessage) {
		if (!(driver.getPageSource().contains(emailErrorMessage))){
			s_assert.fail("Text "+emailErrorMessage +" Expected for Value '"+ DataColX + "'");
			Add_Log.debug("Text "+emailErrorMessage +" Expected for Value '"+ DataColX + "'");
			testfail = true;
		}
		return testfail;
	}
	
	//Compare title of the current page with expected title
	public boolean compareTitle(boolean testfail, String expectedResult, SoftAssert s_assert) {
	//Compare title of the page
			String actualTitle = driver.getTitle();
			if(!(actualTitle.equals(expectedResult))){
				//If expected and actual results not match, Set flag testFail=true.
				testfail=true;	
				//If result Is fail then test failure will be captured Inside s_assert object reference.
				//This soft assertion will not stop your test execution.
				Add_Log.debug("Actual Result Title "+actualTitle+" And Expected Title Value "+expectedResult+" Not Match");
				s_assert.fail("Actual Result Title "+actualTitle+" And Expected Title Value "+expectedResult+" Not Match");
			}
	return testfail;
	}
	
	//check that field format is correct
	public boolean verifyTextFieldPattern(SoftAssert s_assert, boolean testFail, String DataColX, String pattern, String errorMessage) {
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
        java.util.regex.Matcher m = p.matcher(DataColX);
        if (!m.matches()){
        	if (!(driver.getPageSource().contains(errorMessage))){
    			s_assert.fail("Error text "+errorMessage +" Expected for Value '"+ DataColX + "'");
    			Add_Log.debug("Error text "+errorMessage +" Expected for Value '"+DataColX + "'");
    			testFail = true;
        	}
        }
        return testFail;
	}
	
	//verify that actual field equals expected result
	public boolean verifyActualFieldValueandExpected(SoftAssert s_assert, boolean testFail, String ElementId, String expectedValue) {
		String actualValue = getElementByID(ElementId).getAttribute("value");
		if(!(actualValue.equals(expectedValue))){
			//If expected and actual results not match, Set flag testFail=true.
			testFail=true;	
			//If result Is fail then test failure will be captured Inside s_assert object reference.
			//This soft assertion will not stop your test execution.
			Add_Log.debug("Actual Result "+actualValue+" And Expected Value "+expectedValue+" Not Match");
			s_assert.fail("Actual Result "+actualValue+" And Expected Value "+expectedValue+" Not Match");
		}
	return testFail;
	}
	
	//login to the system
	public boolean loginToTheSystemWithValidation(SoftAssert s_assert, boolean testFail, String DataCol1, String DataCol2, String expectedResult) {
		//To navigate to URL. It will read site URL from Param.properties file	
		openWebBrowser("ChatGUI/login/login.view.html");
		//check that login page opened
		testFail = compareTitle(testFail, "Login", s_assert);
		//populate fields with values
		fillLoginField(DataCol1, DataCol2);
		//validate that system is on the correct page
		testFail = compareTitle(testFail, expectedResult, s_assert);
		//return case execution status
		return testFail;
	}

	public void fillLoginField(String DataCol1, String DataCol2) {
		//set values to the fields
		getElementByID("email").sendKeys(DataCol1);
		getElementByID("password").sendKeys(DataCol2);
		getElementByID("btn_loginbutton").click();
	}
	
	public void sleep(){
		try {
			Thread.sleep(Integer.parseInt(Param.getProperty("waitTime")));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
	}
}
