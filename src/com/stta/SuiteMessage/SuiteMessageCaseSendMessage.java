/*
 * 
 * 
 * 
 * 
 * 
 * 
 * 	THIS IS NOT A COMMON TEST SUITE - THIS IS A GO THRU ALL SYSTEM DEMO
 * 	All COMMON METHODS STORED IN LOCAL SUITE REGISTRATION BASE FILE
 * 	NOT AT THE SUITE BASE, EXCEPT THE LOGIN
 * 
 * 	There is almost no validations here, extra sleep methods and so on
 * 
 * 
 * 
 */

//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package com.stta.SuiteMessage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.ToIntFunction;

import org.openqa.selenium.By;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

//So, SuiteAccountCaseModify Class Is Child Class Of SuiteAccountBase Class And SuiteBase Class.
public class SuiteMessageCaseSendMessage extends SuiteMessageBase{
	Read_XLS filePath = null;	
	String sheetName = null;
	String testCaseName = null;	
	String toRunColumnNameTestCase = null;
	String toRunColumnNameTestData = null;
	String testDataToRun[]=null;
	static boolean TestCasePass=true;
	static int dataSet=-1;	
	static boolean testSkip=false;
	static boolean testFail=false;
	SoftAssert s_assert =null;
	
	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();	
		//To set SuiteAccount.xls file's path In filePath Variable.
		filePath = testCaseListExcelFour;		
		testCaseName = this.getClass().getSimpleName();
		//sheetName to check CaseToRun flag against test case.
		sheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		toRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		toRunColumnNameTestData = "DataToRun";
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(testCaseName+" : Execution started.");
		
		//To check test case's CaseToRun = Y or N In related excel sheet.
		//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
		if(!SuiteUtility.checkToRunUtility(filePath, sheetName,toRunColumnNameTestCase,testCaseName)){			
			//To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(filePath, sheetName, "Pass/Fail/Skip", testCaseName, "SKIP");
			//To throw skip exception for this test case.
			throw new SkipException(testCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+testCaseName);
		}
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		testDataToRun = SuiteUtility.checkToRunUtilityOfData(filePath, testCaseName, toRunColumnNameTestData);
	}
	
	//Accepts columns String data In every Iteration.
	@Test(dataProvider="SuiteRegistrationCaseRegData")
	public void SuiteRegistrationCaseRegTest(String DataCol1,String DataCol2,String DataCol3,String DataCol4,String DataCol5,String DataCol6,String expectedResult){
		
		dataSet++;
		
		//Created object of testng SoftAssert class.
		s_assert = new SoftAssert();
				
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){			
			//If DataToRun = "N", Set testSkip=true.
			testSkip=true;
			throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
		
		//To Initialize browser.
		loadWebBrowser();
		
		//To navigate to URL. It will read site URL from Param.properties file	
		openWebBrowser("ChatGUI/registration/registration.view.html");
				
		//Register two users
		testFail = registerUserWithValidation(s_assert, testFail, DataCol1, DataCol2, DataCol5);
		//getElementByLinkText("link_registration").click();
		openWebBrowser("ChatGUI/registration/registration.view.html");
		testFail = registerUserWithValidation(s_assert, testFail, DataCol3, DataCol4, DataCol5);
		
		//Login to the system
		testFail = loginToTheSystemWithValidation(s_assert, testFail, DataCol1, DataCol5, "Index");
		
		//open required iframe - My Account hyperlink
		sleep();
		getElementByXPathStatic("link_contacts").click();
		
		//open frame content
		driver.switchTo().frame("iframeid");
		//to get outside from frame content: driver.switchTo().defaultContent();
		
		//send friendship request
		getElementByID("email").clear();
		getElementByID("email").sendKeys(DataCol3);
		getElementByID("btn_send_friendship_request").click();
		validateMessageExists(s_assert, testFail, DataCol3, "Request sent");
		
		//logout
		driver.switchTo().defaultContent();
		getElementByID("btn_logout").click();
		
		//login as a second user to accept friendship request
		testFail = loginToTheSystemWithValidation(s_assert, testFail, DataCol3, DataCol5, "Index");
		//open required iframe - My Account hyperlink
		sleep();
		getElementByXPathStatic("link_contacts").click();
				
		//open frame content
		driver.switchTo().frame("iframeid");
		//to get outside from frame content: driver.switchTo().defaultContent();
		
		//approve friendship request
		getElementByXPathStatic("btn_approve_friendship_request").click();
		
		//open messages tab
		sleep();
		driver.switchTo().defaultContent();
		sleep();
		getElementByXPathStatic("link_messages").click();
		driver.switchTo().frame("iframeid");
		
		//filter friend list
		getElementByID("filter_friend_in_messages").clear();
		getElementByID("filter_friend_in_messages").sendKeys(DataCol1);
		sleep();
		
		//send message
		//getElementByLinkText(DataCol1).click();
		getElementByCSS("message_friend_email").click();
		sleep();
		getElementByID("message").clear();
		getElementByID("message").sendKeys(DataCol6);
		getElementByID("btn_send_message").click();	
		
		//verify message exists
		getElementByID("message").clear();
		testFail = validateMessageExists(s_assert, testFail, DataCol6 + " (from message box) ", DataCol6);
	
		//logout
		driver.switchTo().defaultContent();
		getElementByID("btn_logout").click();
			
		//login as a first user to view message
		testFail = loginToTheSystemWithValidation(s_assert, testFail, DataCol1, DataCol5, "Index");
		//open required iframe - My Account hyperlink
		sleep();
		getElementByXPathStatic("link_messages").click();
		driver.switchTo().frame("iframeid");
		
		//filter friend list
		getElementByID("filter_friend_in_messages").clear();
		getElementByID("filter_friend_in_messages").sendKeys(DataCol3);
		sleep();
		
		//verify message exists
		getElementByCSS("message_friend_email").click();
		sleep();
		getElementByID("message").clear();
		testFail = validateMessageExists(s_assert, testFail, DataCol6 + " (from message box) ", DataCol6);
	
		//delete created users from DB
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection	("jdbc:mysql://localhost:3306/chat","root","1qaz!QAZ");
			Statement statement = connection.createStatement();
			statement.executeUpdate("delete from users where email = '" + DataCol1 + "' or email = '" + DataCol3 + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	//@AfterMethod method will be executed after execution of @Test method every time.
	@AfterMethod
	public void reporterDataResults(){		
		if(testSkip){
			//If found testSkip = true, Result will be reported as SKIP against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "SKIP");
		}	
		else if(testFail){
			//To make object reference null after reporting In report.
			s_assert = null;
			//Set TestCasePass = false to report test case as fail In excel sheet.
			TestCasePass=false;
			//If found testFail = true, Result will be reported as FAIL against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "FAIL");			
		}
		else{
			//If found testSkip = false and testFail = false, Result will be reported as PASS against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(filePath, testCaseName, "Pass/Fail/Skip", dataSet+1, "PASS");
		}
		//At last make both flags as false for next data set.
		testSkip=false;
		testFail=false;
	}
	
	//This data provider method will return 3 column's data one by one In every Iteration.
	@DataProvider
	public Object[][] SuiteRegistrationCaseRegData(){
		//To retrieve data from Data 1 Column,Data 2 Column and Expected Result column of SuiteAccountCaseModify data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(filePath, testCaseName);
	}

	//To report result as pass or fail for test cases In TestCasesList sheet.
	@AfterTest
	public void closeBrowser(){
		//To Close the web browser at the end of test.
		closeWebBrowserAddLog(testCaseName, TestCasePass, filePath, sheetName);		
	}
	
	
}







/*
dataSet++;

//Created object of testng SoftAssert class.
s_assert = new SoftAssert();		

//If found DataToRun = "N" for data set then execution will be skipped for that data set.
if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){
	//If DataToRun = "N", Set testSkip=true.
	testSkip=true;
	throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
}

//If found DataToRun = "Y" for data set then bellow given lines will be executed.
//To Convert data from String to Integer
int ValueOne = Integer.parseInt(DataCol1);
int ValueTwo = Integer.parseInt(DataCol2);
int ValueThree = Integer.parseInt(DataCol3);
int ExpectedResultInt =  Integer.parseInt(ExpectedResult);
		
//To Initialize Firefox browser.
loadWebBrowser();

driver.get(Param.getProperty("siteURL")+"/2014/04/calc.html");		
getElementByName("txt_Result").clear();
getElementByXPath("btn_Calc_PrePart",ValueOne,"btn_Calc_PostPart").click();
getElementByID("btn_Minus").click();
getElementByXPath("btn_Calc_PrePart",ValueTwo,"btn_Calc_PostPart").click();
getElementByID("btn_Minus").click();
getElementByXPath("btn_Calc_PrePart",ValueThree,"btn_Calc_PostPart").click();
getElementByCSS("btn_Equals").click();
String Result = getElementByName("txt_Result").getAttribute("value");
int ActualResultInt =  Integer.parseInt(Result);
if(!(ActualResultInt==ExpectedResultInt)){
	testFail=true;	
	s_assert.assertEquals(ActualResultInt, ExpectedResultInt, "ActualResult Value "+ActualResultInt+" And ExpectedResult Value "+ExpectedResultInt+" Not Match");
}

if(testFail){
	s_assert.assertAll();		
}	*/		



/*
WebElement email = driver.findElement(By.id("Email"));
email.clear();
email.sendKeys("TestSelenium");
//Simple calc test.

//Locate element by dynamic xPath example.
getElementByXPath("btn_Calc_PrePart",ValueOne,"btn_Calc_PostPart").click();

//Locate Element by ID Locator example.
getElementByID("btn_Plus").click();

getElementByXPath("btn_Calc_PrePart",ValueTwo,"btn_Calc_PostPart").click();

getElementByID("btn_Plus").click();
		
getElementByXPath("btn_Calc_PrePart",ValueThree,"btn_Calc_PostPart").click();

//Locate Element by cssSelector Locator example.
getElementByCSS("btn_Equals").click();


getElementByName("txt_Result").clear();
		getElementByXPath("btn_Calc_PrePart",ValueOne,"btn_Calc_PostPart").click();
		getElementByID("btn_multiply").click();
		getElementByXPath("btn_Calc_PrePart",ValueTwo,"btn_Calc_PostPart").click();
		getElementByCSS("btn_Equals").click();
*/



/*

String Result = getElementByName("txt_Result").getAttribute("value");
int ActualResultInt =  Integer.parseInt(Result);
if(!(ActualResultInt==expectedResultInt)){
	testFail=true;	
	s_assert.assertEquals(ActualResultInt, expectedResultInt, "ActualResult Value "+ActualResultInt+" And ExpectedResult Value "+expectedResultInt+" Not Match");
}

if(testFail){
	//At last, test data assertion failure will be reported In testNG reports and It will mark your test data, test case and test suite as fail.
	s_assert.assertAll();		
}

*/