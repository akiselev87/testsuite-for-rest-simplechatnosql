Selenium  Webdriver automated test suite for chat application

- TestNG and Java

- Webdriver is a main test library

- JDBC to work with DB (clean up test data)

- ANT to display test results

- Test Suites and data stores as xls files and their status updates with every run

- Easy to use with Jenkins