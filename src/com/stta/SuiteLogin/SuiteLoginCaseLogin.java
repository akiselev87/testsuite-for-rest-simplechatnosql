//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package com.stta.SuiteLogin;

import java.io.IOException;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

//SuiteLoginCaseOne Class Inherits From SuiteLoginBase Class.
//So, SuiteLoginCaseOne Class Is Child Class Of SuiteLoginBase Class And SuiteBase Class.

public class SuiteLoginCaseLogin extends SuiteLoginBase{
	Read_XLS filePath = null;
	String sheetName = null;
	String testCaseName = null;	
	String toRunColumnNameTestCase = null;
	String toRunColumnNameTestData = null;
	String testDataToRun[]=null;
	static boolean testCasePass=true;
	static int dataSet=-1;	
	static boolean testSkip=false;
	static boolean testFail=false;
	SoftAssert s_assert =null;	
	
	@BeforeTest
	public void checkCaseToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		//To set SuiteLogin.xls file's path In filePath Variable.
		filePath = testCaseListExcelOne;		
		testCaseName = this.getClass().getSimpleName();	
		//sheetName to check CaseToRun flag against test case.
		sheetName = "TestCasesList";
		//Name of column In TestCasesList Excel sheet.
		toRunColumnNameTestCase = "CaseToRun";
		//Name of column In Test Case Data sheets.
		toRunColumnNameTestData = "DataToRun";
		//Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(testCaseName+" : Execution started.");
		//To retrieve DataToRun flags of all data set lines from related test data sheet.
		testDataToRun = runOrSkipCase(filePath, testCaseName, sheetName, toRunColumnNameTestCase, toRunColumnNameTestData);
	}
	
	//Accepts 4 column's String data In every Iteration.
	@Test(dataProvider="SuiteLoginCaseLoginData")
	public void suiteLoginCaseLoginTest(String DataCol1,String DataCol2,String DataCol3,String expectedResult){
		
		dataSet++;
		
		//Created object of testng SoftAssert class.
		s_assert = new SoftAssert();
		
		//If found DataToRun = "N" for data set then execution will be skipped for that data set.
		if(!testDataToRun[dataSet].equalsIgnoreCase("Y")){	
			Add_Log.info(testCaseName+" : DataToRun = N for data set line "+(dataSet+1)+" So skipping Its execution.");
			//If DataToRun = "N", Set testSkip=true.
			testSkip=true;
			throw new SkipException("DataToRun for row number "+dataSet+" Is No Or Blank. So Skipping Its Execution.");
		}
		
		//If found DataToRun = "Y" for data set then bellow given lines will be executed.
		
		/*Test input data
		*DataCol1
		*DataCol2
		*DataCol3
		*ExpectedResult
		*/
		
		//To Initialize browser.
		loadWebBrowser();		
		
		testFail = loginToTheSystemWithValidation(s_assert, testFail, DataCol1, DataCol2, expectedResult);
		
	}

	//@AfterMethod method will be executed after execution of @Test method every time.
	@AfterMethod
	public void reporterDataResults(){		
		afterMethodCheckStatus(testSkip, testFail, filePath, testCaseName, dataSet, s_assert, testCasePass);
		//At last make both flags as false for next data set.
		testSkip=false;
		testFail=false;
	}
	
	//This data provider method will return 4 column's data one by one In every Iteration.
	@DataProvider
	public Object[][] SuiteLoginCaseLoginData(){
		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteLoginCaseOne data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(filePath, testCaseName);
	}	
	
	//To report result as pass or fail for test cases In TestCasesList sheet.
	@AfterTest
	public void closeBrowser(){
		//To Close the web browser at the end of test.
		closeWebBrowserAddLog(testCaseName, testCasePass, filePath, sheetName);
	}
}
