package com.stta.SuiteLogin;

import java.io.IOException;

import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import com.stta.TestSuiteBase.SuiteBase;
import com.stta.utility.Read_XLS;

public class SuiteLoginBase extends SuiteBase{
	
	//This function will be executed before SuiteLogin's test cases to check SuiteToRun flag.
	@BeforeSuite
	public void checkSuiteToRun() throws IOException{		
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();			
		//To set TestSuiteList.xls file's path In filePath Variable.
		Read_XLS filePath = testSuiteListExcel;
		String sheetName = "SuitesList";
		String suiteName = "SuiteLogin";
		String toRunColumnName = "SuiteToRun";
		runOrSkipSuite(filePath, sheetName, suiteName, toRunColumnName);		
	}		
}



