package com.stta.SuiteMessage;

import java.io.IOException;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import com.stta.TestSuiteBase.SuiteBase;
import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class SuiteMessageBase extends SuiteBase{
	//This function will be executed before SuiteRegistration's test cases to check SuiteToRun flag.
	@BeforeSuite
	public void checkSuiteToRun() throws IOException{
		//Called init() function from SuiteBase class to Initialize .xls Files
		init();	
		//To set TestSuiteList.xls file's path In filePath Variable.
		Read_XLS filePath = testSuiteListExcel;
		String sheetName = "SuitesList";
		String suiteName = "SuiteMessage";
		String toRunColumnName = "SuiteToRun";
		//Called runOrSkipSuite function to run or skip suite
		runOrSkipSuite(filePath, sheetName, suiteName, toRunColumnName);		
		}	
	
	public boolean registerUserWithValidation(SoftAssert s_assert, boolean testFail,String DataCol1, String DataCol2, String DataCol5) {
		testFail = compareTitle(testFail, "Registration", s_assert);
		
		//set values to the fields
		getElementByID("first_name").sendKeys(DataCol2);
		getElementByID("last_name").sendKeys(DataCol2);
		getElementByID("email").sendKeys(DataCol1);
		getElementByID("password").sendKeys(DataCol5);
		getElementByID("password_confirm").sendKeys(DataCol5);
		//click on the register button
		getElementByID("btn_register_user").click();
		
		//click ok on the alert pop up window
		String alertText = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		Add_Log.debug("Alert accepted: " + alertText);
		
		//validate that system is on the correct page
		testFail = compareTitle(testFail, "Login", s_assert);
		return testFail;
	}
}
